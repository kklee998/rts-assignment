package sequence;

public class PowerPlant {

	int coal = 0;
	int currentTemp = 24;
	int currentPressure = 1;

	boolean dangerousTemperature = false;
	boolean dangerousPressure = false;
	boolean fullLoad = false;

	public static void main(String[] args) {

		System.out.println("STARTING THE SEQUENCE POWER PLANT \n\n\n\n");

		PowerPlant powerPlant = new PowerPlant();
		Loader loader = new Loader();
		Burner burner = new Burner();
		Sensor sensors = new Sensor();

		while (true) {
			try {

				if (!powerPlant.fullLoad) {
					powerPlant.coal = loader.load(powerPlant.coal);
				} else {
					loader.dump(powerPlant);
				}

				if (powerPlant.dangerousPressure) {
					burner.releaseSteam(powerPlant);
				}

				if (powerPlant.dangerousTemperature) {
					burner.cool(powerPlant);
				}

				if (powerPlant.coal > 0) {

					powerPlant = burner.burn(powerPlant);
				}

				sensors.sense(powerPlant);

				Thread.sleep(500);
			} catch (Exception e) {

			}

		}

	}
}
