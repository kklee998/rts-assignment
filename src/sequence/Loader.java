package sequence;

public class Loader {

	PowerPlant p;

	public int load(int coal) {
		coal += 3;
		System.out.println("Loading 1 coal. Coal is at:  " + coal);
		System.out.println("==================================");
		return coal;

	}

	public void dump(PowerPlant p) {
		p.coal -= 5;
		p.fullLoad = false;
		System.out.println("DUMPING EXCESS COAL");
	}
}
