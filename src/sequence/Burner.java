package sequence;

public class Burner {

	PowerPlant p;

	public PowerPlant burn(PowerPlant p) {

		System.out.println("**************************************");
		System.out.println("Burning 1 coal. Coal left:  " + p.coal);
		p.coal -= 1;

		p.currentTemp += 20;
		System.out.println("Temperature increased to:  " + p.currentTemp);

		p.currentPressure += 1;
		System.out.println("Pressure increased to:  " + p.currentPressure);
		System.out.println("**************************************");

		return p;

	}

	public void cool(PowerPlant p) {
		while (p.currentTemp >= 200) {
			try {
				System.out.println("COOLING POWER PLANT DOWN");
				p.currentTemp -= 20;

				Thread.sleep(250);

			} catch (Exception e) {

			}
		}

		p.dangerousTemperature = false;
	}

	public void releaseSteam(PowerPlant p) {
		while (p.currentPressure > 4) {
			try {
				System.out.println("RELEASING PRESSURE");
				p.currentPressure -= 1;
				p.currentTemp -= 5;

				Thread.sleep(250);

			} catch (Exception e) {

			}
		}

		p.dangerousPressure = false;
	}

}
