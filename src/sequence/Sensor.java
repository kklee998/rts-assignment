package sequence;

public class Sensor {
	int maxTemp = 400;
	int maxCoal = 5;
	int maxPressure = 15;

	PowerPlant p;

	public void sense(PowerPlant p) {
		if (p.currentTemp >= maxTemp) {
			System.out.println("TEMPERATURE IS TOO HOT!");
			p.dangerousTemperature = true;
		}

		if (p.currentPressure >= maxPressure) {
			System.out.println("PRESSURE IS TOO HIGH");
			p.dangerousPressure = true;
		}

		if (p.coal >= maxCoal) {
			System.out.println("TOO MUCH COAL");
			p.fullLoad = true;
		}
	}

}
