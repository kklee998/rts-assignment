package realtime;

import javax.realtime.AsynchronouslyInterruptedException;
import javax.realtime.Interruptible;
import javax.realtime.RealtimeThread;

public class Loader extends RealtimeThread {

	PowerPlant p;

	public Loader(PowerPlant powerPlant) {
		this.p = powerPlant;
	}

	public void run() {
		AsynchronouslyInterruptedException aie = new AsynchronouslyInterruptedException();
		NormalLoader nl = new NormalLoader(p, aie);
		while (true) {
			try {

				aie.doInterruptible(nl);

				waitForNextRelease();
			} catch (Exception e) {

			}

		}

	}

	public void load() {
		p.coal += 1;
		System.out.println("*************************************");
		System.out.println("COAL LOADED, COAL IS AT:   " + p.coal);

	}

}

class NormalLoader implements Interruptible {
	PowerPlant p;
	AsynchronouslyInterruptedException aie;

	public NormalLoader(PowerPlant p, AsynchronouslyInterruptedException aie) {
		this.p = p;
		this.aie = aie;
	}

	@Override
	public void interruptAction(AsynchronouslyInterruptedException exception) {
		// TODO Auto-generated method stub
		System.out.println("####################");
		System.out.println("DUMPING COAL!!!");
		p.coal -= 2;
		p.fullLoad = false;

	}

	@Override
	public void run(AsynchronouslyInterruptedException exception) throws AsynchronouslyInterruptedException {
		// TODO Auto-generated method stub
		if (!p.fullLoad) {
			p.coal += 1;
			System.out.println("*************************************");
			System.out.println("COAL LOADED, COAL IS AT:   " + p.coal);
		} else {
			System.out.println("COAL IS FULL");
			aie.fire();
		}

	}

}
