package realtime;

import javax.realtime.PeriodicParameters;
import javax.realtime.RelativeTime;
import javax.realtime.ReleaseParameters;

public class PowerPlant {

	int coal = 0;
	int currentTemp = 24;
	int currentPressure = 1;

	boolean dangerousTemperature = false;
	boolean dangerousPressure = false;
	boolean fullLoad = false;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PowerPlant powerPlant = new PowerPlant();
		Sensor sensor = new Sensor(powerPlant);
		Loader loader = new Loader(powerPlant);
		Burner burner = new Burner(powerPlant);


		ReleaseParameters sensorTimer = new PeriodicParameters(new RelativeTime(500, 0));
		ReleaseParameters loaderTimer = new PeriodicParameters(new RelativeTime(750, 0));
		ReleaseParameters burnerTimer = new PeriodicParameters(new RelativeTime(1000, 0));

		sensor.setReleaseParameters(sensorTimer);
		sensor.start();

		loader.setReleaseParameters(loaderTimer);
		loader.start();

		burner.setReleaseParameters(burnerTimer);
		burner.start();

	}

}