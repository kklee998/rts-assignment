package realtime;

import javax.realtime.AsynchronouslyInterruptedException;
import javax.realtime.Interruptible;
import javax.realtime.RealtimeThread;

public class Burner extends RealtimeThread {

	PowerPlant p;

	public Burner(PowerPlant powerPlant) {
		this.p = powerPlant;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		AsynchronouslyInterruptedException burner_aie = new AsynchronouslyInterruptedException();
		NormalBurner nb = new NormalBurner(p, burner_aie);

		while (true) {
			try {
				burner_aie.doInterruptible(nb);
				waitForNextRelease();

			} catch (Exception e) {

			}

		}

	}
}

class NormalBurner implements Interruptible {

	PowerPlant p;
	AsynchronouslyInterruptedException aie;

	public NormalBurner(PowerPlant p, AsynchronouslyInterruptedException aie) {
		this.p = p;
		this.aie = aie;
	}

	@Override
	public void interruptAction(AsynchronouslyInterruptedException exception) {
		// TODO Auto-generated method stub
		if (p.dangerousPressure) {
			while (p.currentPressure >= 5) {
				try {
					System.out.println("####################");
					p.currentTemp -= 5;
					p.currentPressure -= 1;
					System.out.println("RELEASING STEAM!!");
					System.out.println("PRESSURE AT:    " + p.currentPressure);
					System.out.println("TEMPERATURE AT:   " + p.currentTemp);

				} catch (Exception e) {

				}
			}
			p.dangerousPressure = false;
			System.out.println("Pressure is normal again \n\n");
		}

		if (p.dangerousTemperature) {
			while (p.currentTemp >= 200) {
				try {
					System.out.println("####################");
					p.currentTemp -= 30;
					System.out.println("COOLING PLANT DOWN!! TEMPERATURE AT:   " + p.currentTemp);
					Thread.sleep(100);

				} catch (Exception e) {

				}
			}
			p.dangerousTemperature = false;
			System.out.println("Temperature is normal again \n\n");
		}

	}

	@Override
	public void run(AsynchronouslyInterruptedException exception) throws AsynchronouslyInterruptedException {
		// TODO Auto-generated method stub
		if (!p.dangerousPressure && !p.dangerousTemperature) {
			if (p.coal > 0) {
				p.currentPressure += 1;
				p.currentTemp += 20;
				p.coal -= 1;
				System.out.println("========================================");
				System.out.println("COAL BURNED!!! COAL IS AT:   " + p.coal);
				System.out.println("TEMPERATURE IS AT:   " + p.currentTemp);
				System.out.println("PRESSURE IS AT:    " + p.currentPressure);
				System.out.println("========================================");
			} else {
				System.out.println("NO MORE COAL!!");
			}

		} else {
			System.out.println("EMERGENCY TOGGLED");
			aie.fire();
		}

	}

}
