package realtime;

import java.util.concurrent.ExecutionException;

import javax.realtime.RealtimeThread;

public class Sensor extends RealtimeThread {

	PowerPlant p;
	int maxCoal = 5;
	int maxTemp = 300;
	int maxPressure = 10;

	public Sensor(PowerPlant powerPlant) {
		this.p = powerPlant;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (true) {
			try {
				senseCoal();
				senseTemp();
				sensePressure();
				waitForNextRelease();

			} catch (Exception e) {

			}

		}

	}

	public void senseCoal() throws InterruptedException, ExecutionException {
		if (p.coal >= maxCoal) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("COAL OVERLOAD!!!");
			p.fullLoad = true;

		}
	}

	public void senseTemp() throws InterruptedException, ExecutionException {
		if (p.currentTemp >= maxTemp) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("TEMPERATURE TOO HIGH!!");
			p.dangerousTemperature = true;

		}
	}

	public void sensePressure() throws InterruptedException, ExecutionException {
		if (p.currentPressure >= maxPressure) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("PRESSURE TOO HIGH");
			p.dangerousPressure = true;

		}
	}

}
