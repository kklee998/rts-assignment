package concurrent;

import java.util.concurrent.Future;

public class Loader implements Runnable {

	PowerPlant p;

	public Loader(PowerPlant p) {
		this.p = p;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			if (!p.fullLoad) {
				load();
			} else {
				CoalEmergency e = new CoalEmergency(p);
				Future<PowerPlant> future = p.powerPlantEmergency.submit(e);
				this.p = future.get();
			}
		} catch (Exception e) {

		}

	}

	public void load() {
		p.coal += 1;
		System.out.println("*************************************");
		System.out.println("COAL LOADED, COAL IS AT:   " + p.coal);

	}

}
