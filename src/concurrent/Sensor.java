package concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Sensor implements Runnable {

	PowerPlant p;

	int maxCoal = 5;
	int maxTemp = 300;
	int maxPressure = 10;

	public Sensor(PowerPlant p) {
		this.p = p;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			senseCoal();
			senseTemp();
			sensePressure();

		} catch (Exception e) {

		}

	}

	public void senseCoal() throws InterruptedException, ExecutionException {
		if (p.coal >= maxCoal) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("COAL OVERLOAD!!!");
			p.fullLoad = true;

		}
	}

	public void senseTemp() throws InterruptedException, ExecutionException {
		if (p.currentTemp >= maxTemp) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("TEMPERATURE TOO HIGH!!");
			p.dangerousTemperature = true;

		}
	}

	public void sensePressure() throws InterruptedException, ExecutionException {
		if (p.currentPressure >= maxPressure) {
			System.out.println("!!!!!!!!!!!!!!!!!!!");
			System.out.println("PRESSURE TOO HIGH");
			p.dangerousPressure = true;
			
		}
	}

}
