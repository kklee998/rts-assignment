package concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PowerPlant {

	int coal = 0;
	int currentTemp = 24;
	int currentPressure = 1;

	boolean dangerousTemperature = false;
	boolean dangerousPressure = false;
	boolean fullLoad = false;

	ExecutorService powerPlantEmergency = Executors.newCachedThreadPool();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("STARTING THE CONCURRENT POWER PLANT \n\n\n\n");
		PowerPlant powerPlant = new PowerPlant();

		ScheduledExecutorService powerPlantBurner = Executors.newScheduledThreadPool(1);
		ScheduledExecutorService powerPlantLoader = Executors.newScheduledThreadPool(1);
		ScheduledExecutorService powerPlantSensor = Executors.newScheduledThreadPool(1);

		Loader loader = new Loader(powerPlant);
		Sensor sensor = new Sensor(powerPlant);
		Burner burner = new Burner(powerPlant);

		powerPlantSensor.scheduleAtFixedRate(sensor, 0, 500, TimeUnit.MILLISECONDS);
		powerPlantLoader.scheduleAtFixedRate(loader, 0, 750, TimeUnit.MILLISECONDS);
		powerPlantBurner.scheduleAtFixedRate(burner, 0, 1000, TimeUnit.MILLISECONDS);

	}

}

class TempEmergency implements Callable<PowerPlant> {

	PowerPlant p;

	public TempEmergency(PowerPlant p) {
		this.p = p;
	}

	@Override
	public PowerPlant call() throws Exception {
		while (p.currentTemp >= 200) {
			try {
				System.out.println("####################");
				p.currentTemp -= 30;
				System.out.println("COOLING PLANT DOWN!! TEMPERATURE AT:   " + p.currentTemp);
				Thread.sleep(100);

			} catch (Exception e) {

			}
		}
		p.dangerousTemperature = false;
		System.out.println("Temperature is normal again \n\n");
		return p;
	}
}

class CoalEmergency implements Callable<PowerPlant> {

	PowerPlant p;

	public CoalEmergency(PowerPlant p) {
		this.p = p;
	}

	@Override
	public PowerPlant call() throws Exception {
		System.out.println("####################");
		System.out.println("DUMPING COAL!!!");
		p.coal -= 2;
		p.fullLoad = false;
		return p;
	}
}

class PressureEmergency implements Callable<PowerPlant> {

	PowerPlant p;

	public PressureEmergency(PowerPlant p) {
		this.p = p;
	}

	@Override
	public PowerPlant call() throws Exception {
		while (p.currentPressure >= 5) {
			try {
				System.out.println("####################");
				p.currentTemp -= 5;
				p.currentPressure -= 1;
				System.out.println("RELEASING STEAM!!");
				System.out.println("PRESSURE AT:    " + p.currentPressure);
				System.out.println("TEMPERATURE AT:   " + p.currentTemp);
				Thread.sleep(100);

			} catch (Exception e) {

			}
		}
		p.dangerousPressure = false;
		System.out.println("Pressure is normal again \n\n");
		return p;
	}
}
