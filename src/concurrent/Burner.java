package concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Burner implements Runnable {

	PowerPlant p;

	public Burner(PowerPlant p) {
		this.p = p;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (!p.dangerousPressure && !p.dangerousTemperature) {
			burn();
		}

		if (p.dangerousTemperature) {
			TempEmergency e = new TempEmergency(p);
			Future<PowerPlant> future = p.powerPlantEmergency.submit(e);
			try {
				this.p = future.get();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		if (p.dangerousPressure) {
			PressureEmergency e = new PressureEmergency(p);
			Future<PowerPlant> future = p.powerPlantEmergency.submit(e);
			try {
				this.p = future.get();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public void burn() {
		if (p.coal > 0) {
			p.currentPressure += 1;
			p.currentTemp += 20;
			p.coal -= 1;
			System.out.println("========================================");
			System.out.println("COAL BURNED!!! COAL IS AT:   " + p.coal);
			System.out.println("TEMPERATURE IS AT:   " + p.currentTemp);
			System.out.println("PRESSURE IS AT:    " + p.currentPressure);
			System.out.println("========================================");
		} else {
			System.out.println("NO MORE COAL!!");
		}
	}

}
